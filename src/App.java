import models.Employee;
import models.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        // Employee
        Employee employee1 = new Employee();
        Employee employee2 = new Employee(1, "huỳnh", "hoa", 100000);

        System.out.println("Employee 1");   
        System.out.println(employee1.toString());
        System.out.println(employee1.getAnnualSalary());
        System.out.println(employee1.raiseSalary(10));

        System.out.println("Employee 2");
        System.out.println(employee2.toString());
         System.out.println(employee2.getAnnualSalary());
         System.out.println(employee2.raiseSalary(10));
         System.out.println("------------------");

         //InvoiceItem
         InvoiceItem invoiceItem1 = new InvoiceItem();
         InvoiceItem invoiceItem2 = new InvoiceItem("1", "Nước", 2, 12.000);

        System.out.println("InvoiceItem 1:");   
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem1.getTotal());
         System.out.println(invoiceItem1.getDesc());
        

        System.out.println("InvoiceItem 2:");
        
         System.out.println(invoiceItem2.toString());
        System.out.println(invoiceItem2.getTotal());
        System.out.println(invoiceItem2.getDesc());


    }
}
