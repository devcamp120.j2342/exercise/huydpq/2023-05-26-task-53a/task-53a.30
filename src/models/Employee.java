package models;

public class Employee {
    private int id;
    private String firstname;
    private String lastname;
    private int salary;

    public Employee() {
    }

    // khỏi tạo Constructors có tham số
    public Employee(int id, String firstname, String lastname, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    // phương thức getName trả về đầy đủ firstname và lastname
    public String getName(){
        return this.firstname + " " + this.lastname;
    }

    // phương thức getAnnualSalary: Trả ra lương 1 năm
    public int getAnnualSalary(){
        return this.salary * 12;

    }

    // phương thức raiseSalary: Truyền vào % lương tăng thêm.
    public int raiseSalary(int percent){
       int salaryIncrease = (int) (salary * percent / 100.0);
        return salary = salary+ salaryIncrease; 
    }


    @Override
    public String toString() {
        return "Employee [id=" 
        + id + ", firstname=" 
        + firstname + ", lastname=" 
        + lastname + ", salary=" 
        + salary
         + "]";
    }
    
    
}
